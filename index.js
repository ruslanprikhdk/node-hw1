const fs = require('fs');
const path = require('path');
const express = require('express');
const morgan = require('morgan');
const app = express();
const cors = require('cors');
const port = 8080;

app.get('/', function(req, res) {
  res.send('Hello, world!');
});

app.use(express.json());
app.use(cors());
app.use(morgan('combined'));

const allowedExt = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

app.post('/api/files', function(req, res) {
  const fileName = req.body.filename;
  let fileExt = '';
  if (fileName) {
    fileExt = fileName.split('.')[1];
  }   
  try {           
    if (!req.body.content) {
      res.status(400).send({message: "Please specify 'content' parameter"});
    } else if (!req.body.filename) {
      res.status(401).send({message: "Please specify 'filename' parameter"});
    } else if (!allowedExt.includes(fileExt)) {
      res.status(402).send({message: "Please, choose correct file extension"});
    } else {
      fs.writeFile(path.join(__dirname, '/api', '/files', req.body.filename), req.body.content, () => {      
        res.status(200).send({message: "File created successfully"});
      });
    } 
  } catch (err) {
    res.status(500).send({message: "Server error"});  
  }      
});

app.get('/api/files', function(req, res) {
  try {
    if (req.body.filename || req.body.content) {
      res.status(400).send({message: "Client error"});
    } else {
      fs.readdir(path.join(__dirname, '/api', '/files'), (err, files) => {
        res.status(200).send({message: "Success", files: files});
      }); 
    }    
  } catch (err) {
    res.status(500).send({message: "Server error"});
  }   
});


app.get('/api/files/:filename', function(req, res) {
  try {
    const fileName = req.path.substring(11);
  
    fs.readdir(path.join(__dirname, '/api', '/files'), (err, files) => {
      if (!files.includes(fileName)) {
        res.status(400).send({message: `No file with '${fileName}' filename found`});
      } else {
        const uploadDate = fs.statSync(path.join(__dirname, '/api', '/files', fileName)).birthtime;
        fs.readFile(path.join(__dirname, '/api', '/files', fileName), 'utf8', (err, data) => {
          res.status(200).send({
            message: "Success", 
            filename: `${fileName}`,
            content: `${data}`,
            extension: `${fileName.split('.')[1]}`,
            uploadedDate: `${uploadDate.toISOString()}`
          });
        });      
      }
    });
  } catch (err) {
    res.status(500).send({message: "Server error"});
  }
});

app.listen(port);